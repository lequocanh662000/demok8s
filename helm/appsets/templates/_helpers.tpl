{{- define "common.gitsource" -}}
repoURL: https://gitlab.com/lequocanh662000/demok8s.git
targetRevision: {{ $.Values.appVersion }}
{{- end }}

{{- define "common.deploy-destination" -}}
server: {{ .Values.server }}
namespace: {{ .Values.namespace }}
{{- end }}

