package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"

	pb "gitlab.com/lequocanh662000/demoK8s/api"
	chat "gitlab.com/lequocanh662000/demoK8s/client"
	"google.golang.org/grpc"
)

var addr = flag.String("addr", ":8080", "http service address")

var (
	port = flag.Int("port", 50051, "The server port")
)

type server struct {
	pb.UnimplementedGreeterServer
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	pwd, _ := os.Getwd()
	println("root path: %s", pwd)

	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, pwd+"/home.html")
}
func grpcServer() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterGreeterServer(s, &server{})
	log.Printf("grpc server listening at %v\n", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func main() {
	flag.Parse()
	//// gRPC
	go grpcServer()

	//// Websocket
	hub := chat.NewHub()
	go hub.Run()
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		chat.ServeWs(hub, w, r)
	})
	log.Printf("websocket server listening at %s\n", *addr)
	errhttp := http.ListenAndServe(*addr, nil)
	if errhttp != nil {
		log.Fatal("ListenAndServe: ", errhttp)
	}
}

//// gRPC functions

func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	log.Printf("Server Received: %v", in.GetName())
	return &pb.HelloReply{Message: "Hello" + in.GetName()}, nil
}
