###################### CACHED PART #######################
FROM golang:1.19beta1-alpine3.16 as builder

# Add Maintainer info
LABEL maintainer="Anh Lee <lequocanh662000@gmail.com>"

# Make app folder
RUN mkdir /app

# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git

# Set destination for COPY
WORKDIR /app

COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and the go.sum files are not changed 
RUN go mod download all

#COPY all files -> then DOWNLOAD GO MODULES 

COPY . .
#Build
RUN cd /app/server && go build -o ./chatapp

########################### MAIN PART ######################################
FROM alpine:latest
RUN apk --no-cache add ca-certificates
RUN apk add --no-cache bash

RUN mkdir /app
WORKDIR /app

# Add react build assets
# RUN mkdir public
# ADD ./public ./public

# Copy the Pre-built binary file from the previous stage. Observe we also copied the .env file
COPY --from=builder /app/server/chatapp .
COPY .env .

# Expose port 8080 to the outside world
EXPOSE 8080
EXPOSE 50051

# COPY wait-for-mysql.sh /wait-for-mysql.sh
# RUN chmod +x /wait-for-mysql.sh


# CMD [ "/wait-for-mysql.sh","mysql-server:13306", "--", "./chatapp" ]
CMD [ "./chatapp" ]